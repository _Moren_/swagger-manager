import './App.css';

import SelectPage from './components/select-components';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Administrador de visualización de documentación de nuestros servicios hechos en NodeJs.
        </p>
        <SelectPage />
      </header>
    </div>
  );
}

export default App;
