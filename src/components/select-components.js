import { Component } from "react";
import Iframe from 'react-iframe'
const { REACT_APP_SWAGGER_API_PANEL_FLUYAPP, REACT_APP_SWAGGER_ENTITIES, REACT_APP_SWAGGER_DIRECTORIES, REACT_APP_SWAGGER_USERS } = process.env;

class SelectPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urls: [],
      showSite: '',
    };
    this.changeIframe = this.changeIframe.bind(this);
  }

  changeIframe(e) {
    this.setState({ showSite: e.target.value });
  }

  componentDidMount() {
    this.setState({
      urls: [
        { id: REACT_APP_SWAGGER_ENTITIES, name: 'Entidades' },
        { id: REACT_APP_SWAGGER_API_PANEL_FLUYAPP, name: 'Api Panel-Fluyapp' },
        { id: REACT_APP_SWAGGER_DIRECTORIES, name: 'Directorios' },
        { id: REACT_APP_SWAGGER_USERS, name: 'Usuarios' }
      ],
      showSite: REACT_APP_SWAGGER_ENTITIES
    });
  }

  render() {
    const { urls } = this.state;
    const counterPages = urls.length > 0 && urls.map((url, index) => {
      return (
        <option key={index} value={url.id}>{url.name}</option>
      )
    }, this);
    const { showSite } = this.state;
    return (
      <div>
        <select onChange={this.changeIframe}>
          {counterPages}
        </select>
        <div>
          <Iframe url={showSite}
            width="100%"
            height="900px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />
        </div>
      </div>
    )
  }
}

export default SelectPage;